#include "pcproperties.h"
#include <QLayout>
#include <QRegExpValidator>

PcProperties::PcProperties(QSplitter *s, QGraphicsScene *sc, int *dw, int *dh)
{
    scene = sc;
    documentWidth = dw;
    documentHeight = dh;

    properties = new QGroupBox(s);
    properties->setFixedWidth(200);
    QVBoxLayout *vbox = new QVBoxLayout(properties);

    QHBoxLayout *newRectLayout = new QHBoxLayout;
    newRect = new QCheckBox("Соотношение сторон\nновой рамки:", properties);
    newRect->setChecked(0);
    connect(newRect, SIGNAL(toggled(bool)), this, SLOT(setNewRectParameters()));
    newRectWidth = new QSpinBox(properties);
    newRectWidth->setButtonSymbols(QAbstractSpinBox::NoButtons);
    newRectWidth->setFixedWidth(15);
    newRectWidth->setDisabled(1);
    newRectWidth->setMinimum(1);
    connect(newRectWidth, SIGNAL(editingFinished()), this, SLOT(setNewRectParameters()));
    QLabel *dot = new QLabel(":", properties);
    newRectHeight = new QSpinBox(properties);
    newRectHeight->setButtonSymbols(QAbstractSpinBox::NoButtons);
    newRectHeight->setFixedWidth(15);
    newRectHeight->setDisabled(1);
    newRectHeight->setMinimum(1);
    connect(newRectHeight, SIGNAL(editingFinished()), this, SLOT(setNewRectParameters()));

    newRectLayout->addWidget(newRect);
    newRectLayout->addWidget(newRectWidth);
    newRectLayout->addWidget(dot);
    newRectLayout->addWidget(newRectHeight);
    vbox->addLayout(newRectLayout);

    QLabel *title = new QLabel("Параметры выбранной рамки:", properties);
    vbox->addWidget(title);

    nameEditor = new QLineEdit(properties);
    nameEditor->setDisabled(1);
    nameEditor->setValidator(new QRegExpValidator(QRegExp("^[^\/\:\*\?\"\<\>\|\.\\\\]*$"),
                                                  properties));
    vbox->addWidget(nameEditor);
    connect(nameEditor, SIGNAL(editingFinished()), this, SLOT(setSelectedRectName()));

    QGridLayout *parametersLayout = new QGridLayout();

    QLabel *x = new QLabel("X", properties);
    QLabel *y = new QLabel("Y", properties);
    QLabel *w = new QLabel("Ширина", properties);
    QLabel *h = new QLabel("Высота", properties);
    QLabel *fw = new QLabel("Выходная ширина", properties);
    QLabel *fh = new QLabel("Выходная высота", properties);
    parametersLayout->addWidget(x, 0, 0);
    parametersLayout->addWidget(y, 1, 0);
    parametersLayout->addWidget(w, 2, 0);
    parametersLayout->addWidget(h, 3, 0);
    parametersLayout->addWidget(fw, 4, 0);
    parametersLayout->addWidget(fh, 5, 0);

    rectX = new QSpinBox(properties);
    rectX->setButtonSymbols(QAbstractSpinBox::NoButtons);
    rectX->setFixedWidth(60);
    rectX->setDisabled(1);
    rectX->setMaximum(10000);
    connect(rectX, SIGNAL(editingFinished()), this, SLOT(setSelectedRectX()));
    rectY = new QSpinBox(properties);
    rectY->setButtonSymbols(QAbstractSpinBox::NoButtons);
    rectY->setFixedWidth(60);
    rectY->setDisabled(1);
    rectY->setMaximum(10000);
    connect(rectY, SIGNAL(editingFinished()), this, SLOT(setSelectedRectY()));
    rectWidth = new QSpinBox(properties);
    rectWidth->setButtonSymbols(QAbstractSpinBox::NoButtons);
    rectWidth->setFixedWidth(60);
    rectWidth->setDisabled(1);
    rectWidth->setMaximum(10000);
    rectWidth->setMinimum(1);
    connect(rectWidth, SIGNAL(editingFinished()), this, SLOT(setSelectedRectWidth()));
    rectHeight = new QSpinBox(properties);
    rectHeight->setButtonSymbols(QAbstractSpinBox::NoButtons);
    rectHeight->setFixedWidth(60);
    rectHeight->setDisabled(1);
    rectHeight->setMaximum(10000);
    rectHeight->setMinimum(1);
    connect(rectHeight, SIGNAL(editingFinished()), this, SLOT(setSelectedRectHeight()));
    rectFinalWidth = new QSpinBox(properties);
    rectFinalWidth->setButtonSymbols(QAbstractSpinBox::NoButtons);
    rectFinalWidth->setFixedWidth(60);
    rectFinalWidth->setDisabled(1);
    rectFinalWidth->setMaximum(10000);
    rectFinalWidth->setMinimum(1);
    connect(rectFinalWidth, SIGNAL(editingFinished()), this, SLOT(setSelectedRectFinalWidth()));
    rectFinalHeight = new QSpinBox(properties);
    rectFinalHeight->setButtonSymbols(QAbstractSpinBox::NoButtons);
    rectFinalHeight->setFixedWidth(60);
    rectFinalHeight->setDisabled(1);
    rectFinalHeight->setMaximum(10000);
    rectFinalHeight->setMinimum(1);
    connect(rectFinalHeight, SIGNAL(editingFinished()), this, SLOT(setSelectedRectFinalHeight()));
    parametersLayout->addWidget(rectX, 0, 1);
    parametersLayout->addWidget(rectY, 1, 1);
    parametersLayout->addWidget(rectWidth, 2, 1);
    parametersLayout->addWidget(rectHeight, 3, 1);
    parametersLayout->addWidget(rectFinalWidth, 4, 1);
    parametersLayout->addWidget(rectFinalHeight, 5, 1);

    parametersLayout->setColumnMinimumWidth(1, 80);

    vbox->addLayout(parametersLayout);
    vbox->addStretch(1);

    properties->setLayout(vbox);
}

void PcProperties::setRectangle(PcRectangle *r)
{
    rectangle = r;
    if (r == NULL)
    {
        nameEditor->setDisabled(1);
        rectX->setDisabled(1);
        rectY->setDisabled(1);
        rectWidth->setDisabled(1);
        rectHeight->setDisabled(1);
        rectFinalWidth->setDisabled(1);
        rectFinalHeight->setDisabled(1);
    }
    else
    {
        nameEditor->setEnabled(1);
        nameEditor->setText(rectangle->rectangleName());
        rectX->setEnabled(1);
        rectX->setValue(rectangle->coordinateX());
        rectY->setEnabled(1);
        rectY->setValue(rectangle->coordinateY());
        rectWidth->setEnabled(1);
        rectWidth->setValue(rectangle->width());
        rectHeight->setEnabled(1);
        rectHeight->setValue(rectangle->height());
        rectFinalWidth->setEnabled(1);
        rectFinalWidth->setValue(rectangle->finalWidth());
        rectFinalHeight->setEnabled(1);
        rectFinalHeight->setValue(rectangle->finalHeight());
    }
}

void PcProperties::setSelectedRectName()
{
    rectangle->setRectangleName(nameEditor->text());
}

void PcProperties::setSelectedRectX()
{
    if (rectX->value() < (*documentWidth))
        rectangle->setCoordinates(rectX->value(), rectangle->coordinateY());
    scene->update();
}

void PcProperties::setSelectedRectY()
{
    if (rectY->value() < (*documentHeight))
        rectangle->setCoordinates(rectangle->coordinateX(), rectY->value());
    scene->update();
}

void PcProperties::setSelectedRectWidth()
{
    if ((rectX->value() + rectWidth->value()) < (*documentWidth))
    {
        rectangle->setWidth(rectWidth->value());
        rectangle->setFinalWidth(rectFinalWidth->value());
        rectFinalWidth->setValue(rectWidth->value());
    }
    scene->update();
}

void PcProperties::setSelectedRectHeight()
{
    if ((rectY->value() + rectHeight->value()) < (*documentHeight))
    {
        rectangle->setHeight(rectHeight->value());
        rectangle->setFinalHeight(rectHeight->value());
        rectFinalHeight->setValue(rectHeight->value());
    }
    scene->update();
}

void PcProperties::setSelectedRectFinalWidth()
{
    rectangle->setFinalWidth(rectFinalWidth->value());
    scene->update();
}

void PcProperties::setSelectedRectFinalHeight()
{
    rectangle->setFinalHeight(rectFinalHeight->value());
    scene->update();
}

void PcProperties::setNewRectParameters()
{
    if (newRect->isChecked() == 0)
    {
        newRectWidth->setDisabled(1);
        newRectHeight->setDisabled(1);
        emit newProportions(0, 0, 0);
    }
    else
    {
        newRectWidth->setEnabled(1);
        newRectHeight->setEnabled(1);
        emit newProportions(1, newRectWidth->value(), newRectHeight->value());
    }
}

