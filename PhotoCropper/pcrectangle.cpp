#include "pcrectangle.h"
#include <QPainter>

PcRectangle::PcRectangle(double w, double h, double x, double y, QString n, QGraphicsItem *parent):
    QGraphicsItem(parent)
{
    objectWidth = w;
    objectHeight = h;
    objectX = x;
    objectY = y;
    name = n;
    objectFinalWidth = w;
    objectFinalHeight = h;
    selected = 0;
}

void PcRectangle::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QBrush brush;
    QPen pen;

    brush = QBrush (Qt::black, Qt::SolidPattern);
    if (isSelected() == false)
        pen = QPen(brush, 2, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin);
    else
        pen = QPen(brush, 4, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin);
    painter->setPen(pen);
    painter->drawRect(QRectF (objectX, objectY, objectWidth, objectHeight));
}

QRectF PcRectangle::boundingRect() const
{
    if (isSelected() == false)
        return QRectF(objectX - 2, objectY - 2, objectWidth + 2, objectHeight + 2);
    else
        return QRectF(objectX - 3, objectY - 3, objectWidth + 4, objectHeight + 4);
}

