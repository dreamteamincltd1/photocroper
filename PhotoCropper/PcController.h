#ifndef PCCONTROLLER_H
#define PCCONTROLLER_H

#include "pcrectangle.h"
#include "pccropper.h"
#include <QGraphicsScene>
#include <QVector>
#include <QPair>
#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItem>
#include <QSet>


class PcController: public QObject
{
    Q_OBJECT
public:
    PcController(QGraphicsScene *s, QGraphicsView *v, int *dw, int *dh, QWidget *p);
    ~PcController(){}

    void addImage(QImage img, QString name);

private:
    QGraphicsView *mainView = 0;
    QGraphicsScene *scene = 0;
    PcCropper *cropper;
    int *documentWidth;
    int *documentHeight;
    QWidget *parent;
    QVector <PcRectangle *> rectangles;
    QPointF lastPos;
    QPointF firstPos;
    bool leftMouse = 0;
    bool rightMouse = 0;
    PcRectangle *startWid = 0;
    QGraphicsRectItem *newRectArea;
    QImage currentImage;
    QGraphicsPixmapItem *imageItem = NULL;
    QString imageName;
    QString folder;
    bool proportions = 0;
    int proportion1;
    int proportion2;
    bool canCrop = 0;

    bool eventFilter(QObject *, QEvent *event);
    void leftClick();
    void createRectWithArea(QGraphicsRectItem *r);
    void deleteRectangle();
    void switchImage(int direction);

private slots:
    void saveCropped();
    void changeFolder();
    void getNewProportions(bool p, int w, int h);

signals:
    void sendRectangleProperties(PcRectangle *r);
};

#endif // PCCONTROLLER_H
