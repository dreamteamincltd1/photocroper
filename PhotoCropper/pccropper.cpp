#include "pccropper.h"
#include <QFileInfo>
#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QTextStream>

PcCropper::PcCropper(QVector<PcRectangle *> *v, QImage *im, QString *n, QString *f)
{
    rectangles = v;
    image = im;
    imageName = n;
    folder = f;
}

void PcCropper::crop()
{
    QFile info((*folder) + "/" + QFileInfo(*imageName).baseName() + ".txt");
    info.open(QIODevice::Append);
    QTextStream out(&info);

    QImage cropped;
    int num;
    QString outputName;
    for (int i = 0; i < (*rectangles).size(); i++)
    {
        cropped = (*image).copy((*rectangles)[i]->coordinateX(), (*rectangles)[i]->coordinateY(),
                             (*rectangles)[i]->width(),
                                (*rectangles)[i]->height()).scaled((*rectangles)[i]->finalWidth(),
                                                                  (*rectangles)[i]->finalHeight());
        outputName = (*folder) + "/" + (*rectangles)[i]->rectangleName() + "." +
                QFileInfo(*imageName).suffix();
        num = 0;
        while(true)
        {
            if (QFile::exists(outputName))
            {
                num++;
                outputName = (*folder) + "/" + (*rectangles)[i]->rectangleName() + "(" +
                        QString::number(num) + ")" + "." + QFileInfo(*imageName).suffix();
            }
            else
              break;
        }
        cropped.save(outputName);
        out << QFileInfo(outputName).baseName() << " " << (*rectangles)[i]->coordinateX() << " " <<
               (*rectangles)[i]->coordinateY() << "\r\n";
    }
    info.close();

    if (!info.exists())
    {
        folder->clear();
        return;
    }
}
