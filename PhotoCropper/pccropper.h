#ifndef PCCROPPER_H
#define PCCROPPER_H

#include "pcrectangle.h"
#include <QVector>

class PcCropper
{
public:
    PcCropper(QVector <PcRectangle *> *v, QImage *im, QString *n, QString *f);

    void crop();

private:
    QVector <PcRectangle *> *rectangles;
    QImage *image;
    QString *imageName;
    QString *folder;
};

#endif // PCCROPPER_H
