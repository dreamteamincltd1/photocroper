#include "mainwindow.h"

#include <QStyle>
#include <QProcess>
#include <QFileDialog>
#include <QString>
#include <QMessageBox>
#include <QDebug>
#include <QApplication>
#include <QMenuBar>
#include <QScrollBar>
#include <QDir>
#include <QFileInfoList>

mainWindow::mainWindow(QWidget *parent) : QMainWindow(parent)
{
    version = "0.9.1";
    setWindowIcon(QIcon(":/Icons/dog.png"));
    setWindowTitle("PhotoCropper");
    setMinimumSize(800, 600);
    resize(1024, 768);
    documentWidth = new int(1280);
    documentHeight = new int(1024);

    //Компановка основного окна
    horizontalSplitter = new QSplitter(Qt::Horizontal, this);
    setCentralWidget(horizontalSplitter);

    //Рабочая область

    mainScene = new QGraphicsScene(QRectF (0, 0, *documentWidth, *documentHeight));
    mainScene->setBackgroundBrush(Qt::gray);

    mainView = new QGraphicsView (mainScene, horizontalSplitter);
    mainView->setSceneRect(0, 0, *documentWidth, *documentHeight);
    connect(mainView->horizontalScrollBar(), SIGNAL(valueChanged(int)), mainScene, SLOT(update()));
    connect(mainView->verticalScrollBar(), SIGNAL(valueChanged(int)), mainScene, SLOT(update()));

    controller = new PcController(mainScene, mainView, documentWidth, documentHeight, this);

    //Главное меню
    QMainWindow::menuBar();
    //Файл
    file = new QMenu("Файл");
    open = new QAction(QIcon(":/Icons/open.png"), "Открыть изображение", this);
    open->setShortcut(QKeySequence(tr("Ctrl+O")));
    openFolder = new QAction(QIcon(":/Icons/folderOpen.png"), "Открыть папку", this);
    openFolder->setShortcut(QKeySequence(tr("Ctrl+F")));
    save = new QAction(QIcon(":/Icons/save.png"), "Сохранить области", this);
    save->setShortcut(QKeySequence(tr("Ctrl+S")));
    folder = new QAction(QIcon(":/Icons/folder.png"), "Папка для сохранения", this);
    quit = new QAction("Выход", this);
    file->addAction(open);
    file->addAction(openFolder);
    file->addAction(save);
    file->addAction(folder);
    file->addAction(quit);
    menuBar()->addMenu(file);
    //Действия в меню Файл
    connect(quit, &QAction::triggered, this, &mainWindow::close);
    connect(save, SIGNAL(triggered()), controller, SLOT(saveCropped()));
    connect(open, &QAction::triggered, this, &mainWindow::importImage);
    connect(folder, SIGNAL(triggered()), controller, SLOT(changeFolder()));
    connect(openFolder, &QAction::triggered, this, &mainWindow::importImageFolder);


    about = new QMenu("Помощь");
    versionInfo = new QAction("Информация о версии", this);
    about->addAction(versionInfo);
    menuBar()->addMenu(about);
    connect(versionInfo, &QAction::triggered, this, &mainWindow::showInfo);


    properties = new PcProperties(horizontalSplitter, mainScene, documentWidth, documentHeight);

    //Сглаживание
    mainView->setRenderHint(QPainter::Antialiasing);

    connect(controller, SIGNAL(sendRectangleProperties(PcRectangle*)), properties,
            SLOT(setRectangle(PcRectangle*)));
    connect(properties, SIGNAL(newProportions(bool,int,int)), controller,
            SLOT(getNewProportions(bool,int,int)));

}


//Функция импортирования изображения
void mainWindow::importImage(){
    QString fileName = QFileDialog::getOpenFileName(this, "Open image", getenv("HOME"),
                                                    "Image Files (*.jpg *.png *.bmp)");
    if (!fileName.isEmpty())
    {
        QImage img(fileName);
        if (img.isNull())
        {
            QMessageBox::critical(this, "Error","Cannot open image", QMessageBox::Ok);
            return;
        }
        controller->addImage(img, fileName);
    }
}

void mainWindow::importImageFolder()
{
    QString imgFolder = QFileDialog::getExistingDirectory(mainView, tr("Open Directory"),
                                                 "/home", QFileDialog::ShowDirsOnly
                                                          |QFileDialog::DontResolveSymlinks);
    QDir dir(imgFolder);
    QFileInfoList dirContent = dir.entryInfoList(QStringList() << "*.jpg" << "*.png" << "*.bmp",
                                                 QDir::Files | QDir::NoDotAndDotDot);
    int i = 0;
    while (i < dirContent.size())
    {
        if (QImage(dirContent[i].filePath()).isNull())
            i++;
        else
        {
            controller->addImage(QImage(dirContent[i].filePath()), dirContent[i].filePath());
            break;
        }
    }
}

void mainWindow::showInfo()
{
    QMessageBox::about(this, "Info", version);
}

mainWindow::~mainWindow()
{
    delete documentWidth;
    delete documentHeight;
    delete horizontalSplitter;
    delete mainScene;
    delete controller;
    delete file;
    delete open;
    delete save;
    delete quit;
}
