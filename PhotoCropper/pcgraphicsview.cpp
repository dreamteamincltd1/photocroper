#include "pcgraphicsview.h"
#include <QCursor>
#include <QCoreApplication>


PcGraphicsView::PcGraphicsView(QGraphicsScene *scene, QSplitter *splitter, int *dw, int *dh)
    : QGraphicsView(scene, splitter)
{
    mainScene = scene;
    documentWidth = dw;
    documentHeight = dh;
    setResizeAnchor(QGraphicsView::AnchorUnderMouse);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
}


void PcGraphicsView::drawBackground(QPainter *painter, const QRectF &rect)
{
    painter->setPen(QPen(QColor(33, 33, 33, 255), 1, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));
    painter->setBrush(QBrush (Qt::gray, Qt::SolidPattern));
    painter->drawRect(rect);
    painter->setBrush(QBrush (Qt::white, Qt::SolidPattern));
    painter->drawRect(0, 0, *documentWidth, *documentHeight);
}

