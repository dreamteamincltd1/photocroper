#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QGraphicsScene>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QGraphicsView>
#include <QSplitter>
#include <QGroupBox>
#include <QVector>
#include <QMenu>
#include "pccontroller.h"
#include "pcproperties.h"

class mainWindow: public QMainWindow
{
    Q_OBJECT

public:
    mainWindow(QWidget *parent = 0);
    ~mainWindow();

private:
    QString version;
    QGraphicsScene *mainScene;
    QGraphicsView *mainView;
    PcController *controller;
    PcProperties *properties;

    QSplitter *horizontalSplitter;

    int *documentWidth;
    int *documentHeight;

    QMenu *file;
    QAction *open;          //отрыть картинку
    QAction *openFolder;
    QAction *save;          //сохранить размеченный кусок (в мелкие файлы и txt)
    QAction *folder;
    QAction *quit;
    QMenu *about;
    QAction *versionInfo;

private slots:
    void importImage();
    void importImageFolder();
    void showInfo();
};

#endif // MAINWINDOW_H
