#ifndef PCRECTANGLE_H
#define PCRECTANGLE_H

#include <QGraphicsItem>

class PcRectangle: public QGraphicsItem
{
public:
    PcRectangle(double w, double h, double x, double y, QString n, QGraphicsItem *parent = 0);
    ~PcRectangle(){}

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *widget) override;
    virtual QRectF boundingRect() const override;

    void setCoordinates(double x, double y)
    {
        objectX = x;
        objectY = y;
    }

    void setSelected(bool s)
    {
        selected = s;
    }

    void setWidth(double w)
    {
        objectWidth = w;
    }

    void setHeight(double h)
    {
        objectHeight = h;
    }

    void setFinalWidth(double w)
    {
        objectFinalWidth = w;
    }

    void setFinalHeight(double h)
    {
        objectFinalHeight = h;
    }

    void setRectangleName(QString n)
    {
        name = n;
    }


    double width() const
    {
        return objectWidth;
    }

    double height() const
    {
        return objectHeight;
    }


    double coordinateX() const
    {
        return objectX;
    }

    double coordinateY() const
    {
        return objectY;
    }

    bool isSelected() const
    {
        return selected;
    }

    double finalWidth() const
    {
        return objectFinalWidth;
    }

    double finalHeight() const
    {
        return objectFinalHeight;
    }

    QString rectangleName()
    {
        return name;
    }

private:
    double objectX, objectY, objectWidth, objectHeight;
    QString name;
    double objectFinalWidth, objectFinalHeight;
    bool selected;
};

#endif // PCRECTANGLE_H
