#ifndef PCGRAPHICSVIEW_H
#define PCGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QSplitter>
#include <QWheelEvent>

class PcGraphicsView: public QGraphicsView
{
public:
    PcGraphicsView(QGraphicsScene *scene, QSplitter *splitter, int *dw, int *dh);

private:
    void drawBackground(QPainter *painter, const QRectF &rect) override;

    bool stop = 0;
    QGraphicsScene *mainScene;
    int *documentWidth;
    int *documentHeight;

};

#endif // PCGRAPHICSVIEW_H
