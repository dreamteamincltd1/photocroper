#ifndef PCPROPERTIES_H
#define PCPROPERTIES_H

#include "pcrectangle.h"
#include <QSplitter>
#include <QGroupBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QLabel>
#include <QList>
#include <QString>
#include <QColor>
#include <QGraphicsScene>
#include <QCheckBox>


class PcProperties: public QWidget
{
    Q_OBJECT
public:
    PcProperties(QSplitter *s, QGraphicsScene *sc, int *dw, int *dh);

private:
    QGroupBox *properties;
    QLineEdit *nameEditor;
    QSpinBox *rectX;
    QSpinBox *rectY;
    QSpinBox *rectWidth;
    QSpinBox *rectHeight;
    QSpinBox *rectFinalWidth;
    QSpinBox *rectFinalHeight;
    QSpinBox *newRectWidth;
    QSpinBox *newRectHeight;
    QCheckBox *newRect;

    PcRectangle *rectangle;
    QGraphicsScene *scene;
    int *documentWidth;
    int *documentHeight;

private slots:
    void setRectangle(PcRectangle *r);
    void setSelectedRectName();
    void setSelectedRectX();
    void setSelectedRectY();
    void setSelectedRectWidth();
    void setSelectedRectHeight();
    void setSelectedRectFinalWidth();
    void setSelectedRectFinalHeight();
    void setNewRectParameters();

signals:
    void newProportions(bool p, int w, int h);
};

#endif // PCPROPERTIES_H
