#include "pccontroller.h"
#include <QCoreApplication>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QList>
#include <QDebug>
#include <math.h>
#include <QFileInfo>
#include <QFileDialog>
#include <QDir>
#include <QFileInfoList>
#include <QMessageBox>

PcController::PcController(QGraphicsScene *s, QGraphicsView *v, int *dw, int *dh, QWidget *p)
{
    parent = p;
    scene = s;
    mainView = v;
    documentWidth = dw;
    documentHeight = dh;
    folder.clear();
    cropper = new PcCropper(&rectangles, &currentImage, &imageName, &folder);

    scene->installEventFilter(this);
}



bool PcController::eventFilter(QObject *, QEvent *event)
{
    if (event->type() == QEvent::GraphicsSceneMousePress)
    {
        QGraphicsSceneMouseEvent *mouseEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
        lastPos = mouseEvent->scenePos();
        firstPos = lastPos;
        if (firstPos.x() < 0)
            firstPos.setX(0);
        if (firstPos.y() < 0)
            firstPos.setY(0);
        if (firstPos.x() > (*documentWidth))
            firstPos.setX(*documentWidth);
        if (firstPos.y() > (*documentHeight))
            firstPos.setY(*documentHeight);
        if (mouseEvent->button() == Qt::LeftButton)
        {            
            leftMouse = 1;
            QList <QGraphicsItem *> itemsUnderMouse;
            itemsUnderMouse = scene->items(firstPos, Qt::IntersectsItemShape, Qt::DescendingOrder,
                                           QTransform());
            for (QGraphicsItem *w : itemsUnderMouse)
            {
                if (w->zValue() > 1)
                {
                    PcRectangle *temp = static_cast<PcRectangle *>(w);
                    startWid = temp;
                }
                else
                    startWid = 0;
                break;
            }
        }

        if (mouseEvent->button() == Qt::RightButton)
        {
            startWid = 0;
            rightMouse = 1;
            newRectArea = new QGraphicsRectItem(firstPos.x(), firstPos.y(), 0, 0, 0);
            QBrush brush;
            QPen pen;
            brush = QBrush (Qt::black, Qt::SolidPattern);
            pen = QPen(brush, 1, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin);
            newRectArea->setPen(pen);
            newRectArea->setZValue(4);
            scene->addItem(newRectArea);
            scene->update();
        }
    }

    if (event->type() == QEvent::GraphicsSceneMouseRelease)
    {
        QGraphicsSceneMouseEvent *mouseEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
        if (mouseEvent->button() == Qt::LeftButton)
        {
            if (firstPos == mouseEvent->scenePos())
                leftClick();
            leftMouse = 0;
        }

        if ((startWid == 0) && (mouseEvent->button() == Qt::RightButton))
        {
            createRectWithArea(newRectArea);
            scene->removeItem(newRectArea);
            delete newRectArea;
            rightMouse = 0;
            scene->update();
        }
    }

    if (event->type() == QEvent::GraphicsSceneMouseMove)
    {
        QGraphicsSceneMouseEvent *mouseEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
        if (rightMouse)
        {
            QPointF newPos = mouseEvent->scenePos();
            if (newPos.x() < 0)
                newPos.setX(0);
            if (newPos.y() < 0)
                newPos.setY(0);
            if (newPos.x() > (*documentWidth))
                newPos.setX(*documentWidth);
            if (newPos.y() > (*documentHeight))
                newPos.setY(*documentHeight);
            scene->removeItem(newRectArea);
            if (!proportions)
                newRectArea->setRect(firstPos.x(), firstPos.y(), newPos.x() - firstPos.x(),
                                     newPos.y() - firstPos.y());
            else
            {
                double newWidth;
                double newHeight;
                if (abs(firstPos.x() - lastPos.x()) > (abs(firstPos.y() - lastPos.y())))
                {
                    newWidth = newPos.x() - firstPos.x();
                    newHeight = newWidth * proportion2 / proportion1;
                    if ((newHeight + firstPos.y()) < 0)
                    {
                        newHeight = -firstPos.y();
                        newWidth = newHeight * proportion1 / proportion2;
                    }
                    if ((newHeight + firstPos.y()) > (*documentHeight))
                    {
                        newHeight = (*documentHeight) - firstPos.y();
                        newWidth = newHeight * proportion1 / proportion2;
                    }
                }
                else
                {
                    newHeight = newPos.y() - firstPos.y();
                    newWidth = newHeight * proportion1 / proportion2;
                    if ((newWidth + firstPos.x()) < 0)
                    {
                        newWidth = -firstPos.x();
                        newHeight = newWidth * proportion2 / proportion1;
                    }
                    if ((newWidth + firstPos.x()) > (*documentWidth))
                    {
                        newWidth = (*documentWidth) - firstPos.x();
                        newHeight = newWidth * proportion2 / proportion1;
                    }
                }
                newRectArea->setRect(firstPos.x(), firstPos.y(), newWidth, newHeight);
            }
            newRectArea->setZValue(4);
            scene->addItem(newRectArea);
            scene->update();
            lastPos = newPos;
        }
    }

    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);

        if (keyEvent->key() == Qt::Key_Delete)
        {
            deleteRectangle();
        }
        if (keyEvent->key() == Qt::Key_Left)
        {
            switchImage(-1);
        }
        if (keyEvent->key() == Qt::Key_Right)
        {
            switchImage(1);
        }
    }

    return false;
}

void PcController::leftClick()
{
    for (int i = 0; i < rectangles.size(); i++)
    {
        rectangles[i]->setSelected(0);
        rectangles[i]->setZValue(2);
    }


    if (startWid != 0)
    {
        startWid->setSelected(1);
        startWid->setZValue(3);
        emit sendRectangleProperties(startWid);
    }
    else
        emit sendRectangleProperties(NULL);

    scene->update();
}

void PcController::deleteRectangle()
{
    int i = 0;
    int size = rectangles.size();
    while (i < size)
    {
        if (rectangles[i]->isSelected() == 1)
        {
            scene->removeItem(rectangles[i]);
            rectangles.remove(i);
            size--;
        }
        else
            i++;
    }
    scene->update();
}


void PcController::createRectWithArea(QGraphicsRectItem *r)
{
    QString name = QFileInfo(imageName).baseName() + "_cropped";
    double w, h, x, y;
    w = r->rect().width();
    h = r->rect().height();
    x = r->rect().x();
    y = r->rect().y();
    if (w < 0)
    {
        w = abs(w);
        x = x - w;
    }
    if (h < 0)
    {
        h = abs(h);
        y = y - h;
    }

    PcRectangle *rect = new PcRectangle(w, h, x, y, name);
    rect->setZValue(2);
    scene->addItem(rect);
    rectangles.push_back(rect);
    scene->update();
}

void PcController::addImage(QImage img, QString name)
{
    //rectangles.clear();
    //scene->clear();
    canCrop = 1;
    if (imageItem != NULL)
        scene->removeItem(imageItem);

    currentImage = img;
    imageName = name;
    *documentWidth = img.width();
    *documentHeight = img.height();
    imageItem = new QGraphicsPixmapItem(QPixmap::fromImage(img));
    scene->addItem(imageItem);
    mainView->setSceneRect(0, 0, *documentWidth, *documentHeight);

    int i = 0;
    while (i < rectangles.size())
    {
        if (((rectangles[i]->coordinateX() + rectangles[i]->width()) <= img.width()) &&
                ((rectangles[i]->coordinateY() + rectangles[i]->height()) <= img.height()))
        {
            rectangles[i]->setRectangleName(QFileInfo(imageName).baseName() + "_cropped");
            i++;
        }
        else
        {
            scene->removeItem(rectangles[i]);
            rectangles.remove(i);
        }
    }

    scene->update();
}

void PcController::saveCropped()
{
    if (!canCrop)
        return;
    if (rectangles.size() > 0)
    {
        if (!folder.isEmpty())
            cropper->crop();
        else
        {
            changeFolder();
            if (folder.isEmpty())
                return;
            cropper->crop();
        }
    }
    if (folder.isEmpty())
        QMessageBox::critical(parent, "Error","Cannot write to this folder", QMessageBox::Ok);
}

void PcController::changeFolder()
{
    folder = QFileDialog::getExistingDirectory(mainView, tr("Open Directory"), "/home",
                                               QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
}

void PcController::getNewProportions(bool p, int w, int h)
{
    proportions = p;
    proportion1 = w;
    proportion2 = h;
}

void PcController::switchImage(int direction)
{
    QDir dir(QFileInfo(imageName).path());
    QFileInfoList dirContent = dir.entryInfoList(QStringList() << "*.jpg" << "*.png" << "*.bmp",
                                                 QDir::Files | QDir::NoDotAndDotDot);
    if (dirContent.size() > 1)
    {
        saveCropped();
        if (folder.isEmpty())
            return;
        int i = 0;
        while(true)
        {
            if (dirContent[i].filePath() == imageName)
                break;
            else
                i++;
        }
        i += direction;
        if (i == -1)
            i = dirContent.size() - 1;
        if (i == dirContent.size())
            i = 0;
        int nextDirection;
        if (direction < 0)
            nextDirection = -1;
        else
            nextDirection = 1;
        if (QImage(dirContent[i].filePath()).isNull())
            switchImage(direction + nextDirection);
        else
        {
            if (dirContent[i].filePath() != imageName)
                addImage(QImage(dirContent[i].filePath()), dirContent[i].filePath());
            else
                return;
        }
    }
}
