#-------------------------------------------------
#
# Project created by QtCreator 2014-10-12T13:10:12
#
#-------------------------------------------------

QT       += core gui
QT += gui-private
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PhotoCropper

TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    pccontroller.cpp \
    pcrectangle.cpp \
    pccropper.cpp \
    pcproperties.cpp

HEADERS += \
    mainwindow.h \
    pccontroller.h \
    pcrectangle.h \
    pccropper.h \
    pcproperties.h

RESOURCES += \
    res.qrc
